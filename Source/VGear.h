//Serial connection speed
#define SERIAL_SPEED_BAUD 38400
#define VSTROM

#if defined DL1000
//Gear Voltages
#define GEAR1_DEFAULT    1.78f
#define GEAR1_FROM_VOLTS 0.00f
#define GEAR2_FROM_VOLTS 2.32f
#define GEAR3_FROM_VOLTS 2.95f
#define GEAR4_FROM_VOLTS 3.65f
#define GEAR5_FROM_VOLTS 4.12f
#define GEAR6_FROM_VOLTS 4.65f
#define GEARN_FROM_VOLTS 4.85f
#define GEARN_TO_VOLTS   5.50f
#endif
#if defined TLS
//Gear Voltages
#define GEAR1_DEFAULT    1.78f
#define GEAR1_FROM_VOLTS 0.00f
#define GEAR2_FROM_VOLTS 2.12f
#define GEAR3_FROM_VOLTS 3.47f
#define GEAR4_FROM_VOLTS 3.72f
#define GEAR5_FROM_VOLTS 4.12f
#define GEAR6_FROM_VOLTS 4.67f
#define GEARN_FROM_VOLTS 4.85f
#define GEARN_TO_VOLTS   5.50f
#endif

#if defined VSTROM
//Gear Voltages
#define GEAR1_DEFAULT    1.33f
#define GEAR1_FROM_VOLTS 0.00f
#define GEAR2_FROM_VOLTS 1.61f
#define GEAR3_FROM_VOLTS 2.20f
#define GEAR4_FROM_VOLTS 2.90f
#define GEAR5_FROM_VOLTS 3.65f
#define GEAR6_FROM_VOLTS 4.25f
#define GEARN_FROM_VOLTS 4.80f
#define GEARN_TO_VOLTS   5.50f
#endif
//Voltage Divider on the Pink wire 2:1 as netural is over 5v
#define GEAR_VOLT_DIVIDER 2.00f

//Pins for the 7 Segment display
#define SEGMENT_G_PIN 12
#define SEGMENT_F_PIN 11
#define SEGMENT_A_PIN 10
#define SEGMENT_B_PIN 9
#define SEGMENT_E_PIN 6
#define SEGMENT_C_PIN 8
#define SEGMENT_D_PIN 7  

//Pin for the sesor wire
#define ANALOGPIN_GEAR_POSITION A3

#define GEAR_NEUTRAL 0
#define GEAR_ERROR -1

#define IsBetween( x, a, b ) ( x >= a && x <= b ? 1 : 0 )     
#define GEAR_WINDOW_SIZE 12

// msec
#define MIN_TRANSIENTGEAR_INTERVAL 200
