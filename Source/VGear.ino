
/**
* A Simple VSTROM Gear Position Indicator that uses a 7 segment display
* Various projects used for reference 
* -- http://stromputer.wikispaces.com/
* -- http://www.whiskeytangohotel.com/2012/01/suzuki-v-strom-dl1000-gear-position.html
*
* This software is provided as is, with no warranty given or implied
*/

#include <RunningAverage.h>   
#include "VGear.h"
   
float vcc; // Actual VCC in Volts
short gear = 0;               // 0 = Neutral, or 1-6
long transientGearStartMillis = 0;
short transientGear = GEAR_ERROR;
short lastGearLED = -2;       // Force initialb update
bool gearReadError = false;
float gearPositionVolts = 0; 
short LoopSleepTime = 100; // msec


RunningAverage gearLevelRunAvg(GEAR_WINDOW_SIZE); 

void testLEDS() {
  displayNeutral();
  delay(500);
  displayFirst();
  delay(500);
  displaySecond();
  delay(500);
  displayThird();
  delay(500);
  displayFourth();
  delay(500);
  displayFifth();
  delay(500);
  displaySixith();
  delay(500);
}

//10,11,3,2,5,6,7,8
void setup() 
{ 
    // Setup Serial connection
    Serial.begin( SERIAL_SPEED_BAUD );
    
    // Setup all input pins
    pinMode(ANALOGPIN_GEAR_POSITION, INPUT);     // Set Gear Position pin for Input Mode
    digitalWrite(ANALOGPIN_GEAR_POSITION, LOW);  // Disable pullup on analog pin of Gear Position
    
    pinMode(SEGMENT_G_PIN, OUTPUT);
    pinMode(SEGMENT_F_PIN, OUTPUT);
    pinMode(SEGMENT_A_PIN, OUTPUT);
    pinMode(SEGMENT_B_PIN, OUTPUT);
    pinMode(SEGMENT_E_PIN, OUTPUT);
    pinMode(SEGMENT_C_PIN, OUTPUT);
    pinMode(SEGMENT_D_PIN, OUTPUT);
    
    analogReference(DEFAULT);   // Use 5V Reference for analogRead (ADC)
    testLEDS();
    vcc = readVcc();

}

void determineCurrentGear()
{
     short lastTransientGear = transientGear;

     // Calculate fixed gear position volts
     float fixedGearPositionVolts = gearPositionVolts;
     
     if ( IsBetween( fixedGearPositionVolts,  GEAR1_FROM_VOLTS, GEAR2_FROM_VOLTS ) )
         transientGear = 1;
     else if ( IsBetween( fixedGearPositionVolts, GEAR2_FROM_VOLTS, GEAR3_FROM_VOLTS) )
         transientGear = 2;
     else if ( IsBetween( fixedGearPositionVolts , GEAR3_FROM_VOLTS, GEAR4_FROM_VOLTS ) )
         transientGear = 3;
     else if ( IsBetween( fixedGearPositionVolts, GEAR4_FROM_VOLTS, GEAR5_FROM_VOLTS ) )
         transientGear = 4;
     else if ( IsBetween( fixedGearPositionVolts, GEAR5_FROM_VOLTS, GEAR6_FROM_VOLTS ) )
         transientGear = 5;
     else if ( IsBetween( fixedGearPositionVolts, GEAR6_FROM_VOLTS, GEARN_FROM_VOLTS ) )
         transientGear = 6;
     else if ( IsBetween( fixedGearPositionVolts, GEARN_FROM_VOLTS, GEARN_TO_VOLTS ) )
         transientGear = GEAR_NEUTRAL;
     else
         transientGear = GEAR_ERROR; // Default: Error
         
     // If transient gear has changed, do not allow it to stabilize
     if ( lastTransientGear != transientGear )
     {
         // Reset the transient start counter
         transientGearStartMillis = millis();
         lastTransientGear = transientGear;
     }
     else // Transient gear is the same since last time
     {
         int deltaTransientMillis = millis() - transientGearStartMillis;
         // check if it was stable long enough
         if ( deltaTransientMillis > MIN_TRANSIENTGEAR_INTERVAL )
         {
             // Make transient gear change a stable gear change
             gear = transientGear;

             transientGear = GEAR_ERROR;
         }
     }
}

void readGearPositionAnalog()
{
    gearReadError = true; // Assume read had errors, by default
   
    int rawValue = analogRead( ANALOGPIN_GEAR_POSITION );

    float currentGearPositionVolts = GEAR_VOLT_DIVIDER * vcc * ( rawValue / 1024.0f );    // read the input pin for Gear Position
    // Constrain the input reading, to reduce errors
    currentGearPositionVolts = constrain( currentGearPositionVolts, 0.01f, 5.0f );   
    if ( abs( currentGearPositionVolts - gearLevelRunAvg.getAverage() ) > 0.5f ) // big change, probably a real gear shift
    {
        gearLevelRunAvg.trimToValue( currentGearPositionVolts );
    }
    else // small change
    {    
        // Recalc average volts
        gearLevelRunAvg.addValue( currentGearPositionVolts );
    }

    // Use the average gear volts
    gearPositionVolts = gearLevelRunAvg.getAverage();
    
    determineCurrentGear();
    
    gearReadError = false; // Clear read error - we made it here
}

//Display a -
void displayNeutral() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, LOW);
    digitalWrite(SEGMENT_A_PIN, LOW);
    digitalWrite(SEGMENT_B_PIN, LOW);
    digitalWrite(SEGMENT_E_PIN, LOW);
    digitalWrite(SEGMENT_C_PIN, LOW);
    digitalWrite(SEGMENT_D_PIN, LOW);
}

//Display 1
void displayFirst() {
    digitalWrite(SEGMENT_G_PIN, LOW);
    digitalWrite(SEGMENT_F_PIN, LOW);
    digitalWrite(SEGMENT_A_PIN, LOW);
    digitalWrite(SEGMENT_B_PIN, HIGH);
    digitalWrite(SEGMENT_E_PIN, LOW);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, LOW);
}

//Display 2
void displaySecond() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, LOW);
    digitalWrite(SEGMENT_A_PIN, HIGH);
    digitalWrite(SEGMENT_B_PIN, HIGH);
    digitalWrite(SEGMENT_E_PIN, HIGH);
    digitalWrite(SEGMENT_C_PIN, LOW);
    digitalWrite(SEGMENT_D_PIN, HIGH);
}

//Display 3
void displayThird() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, LOW);
    digitalWrite(SEGMENT_A_PIN, HIGH);
    digitalWrite(SEGMENT_B_PIN, HIGH);
    digitalWrite(SEGMENT_E_PIN, LOW);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, HIGH);
}

//Display 4
void displayFourth() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, HIGH);
    digitalWrite(SEGMENT_A_PIN, LOW);
    digitalWrite(SEGMENT_B_PIN, HIGH);
    digitalWrite(SEGMENT_E_PIN, LOW);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, LOW);
}

//Display 5
void displayFifth() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, HIGH);
    digitalWrite(SEGMENT_A_PIN, HIGH);
    digitalWrite(SEGMENT_B_PIN, LOW);
    digitalWrite(SEGMENT_E_PIN, LOW);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, HIGH);
}

//Display 6
void displaySixith() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, HIGH);
    digitalWrite(SEGMENT_A_PIN, HIGH);
    digitalWrite(SEGMENT_B_PIN, LOW);
    digitalWrite(SEGMENT_E_PIN, HIGH);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, HIGH);
}

void allOn() {
    digitalWrite(SEGMENT_G_PIN, HIGH);
    digitalWrite(SEGMENT_F_PIN, HIGH);
    digitalWrite(SEGMENT_A_PIN, HIGH);
    digitalWrite(SEGMENT_B_PIN, HIGH);
    digitalWrite(SEGMENT_E_PIN, HIGH);
    digitalWrite(SEGMENT_C_PIN, HIGH);
    digitalWrite(SEGMENT_D_PIN, HIGH);
}


void updateGearDisplay()
{
    // Optimization: Don't print the gear position if nothing has changed since the last printing
    if ( !gearReadError && ( lastGearLED == gear ) )
    {
        return;
    }
    
    lastGearLED = gear;
    
    // Update gear, only if not in error mode
    if ( gear != GEAR_ERROR )
    {
       Serial.print("Gear Changed to: ");
       Serial.println(gear);
       switch (gear) {
         case 1:
             displayFirst();
             break;
         case 2:
             displaySecond();
             break;
         case 3:
             displayThird();
             break;
         case 4:
             displayFourth();
             break;
         case 5:
             displayFifth();
             break;
         case 6:
             displaySixith();
             break;
         default:
             displayNeutral();
            
       }
    }
}

/// ----------------------------------------------------------------------------------------------------
/// Calculates the on board VCC, which is needed for accurate ADC readings 
/// analogRead reads from GND..VCC in 1024 steps
/// Source: http://hacking.majenko.co.uk/node/57
/// ----------------------------------------------------------------------------------------------------
float readVcc() {
    long result;
    // Read 1.1V reference against AVcc
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(2); // Wait for Vref to settle
    ADCSRA |= _BV(ADSC); // Convert
    while (bit_is_set(ADCSRA,ADSC));
    result = ADCL;
    result |= ADCH<<8;
    result = 1125300L / result; // Back-calculate AVcc in mV
    
    return result / 1000.0f;
}

void loop() {
    vcc = readVcc();
    readGearPositionAnalog(); 
    updateGearDisplay();          
    delay(LoopSleepTime);
}

