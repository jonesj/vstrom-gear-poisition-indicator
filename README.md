# README #

This is a project is a gear position indicator for some motorcycles, namely Suzuki DL650, but can also be compiled for the Suzuki DL1000/ Kawasaki KLE1000/ Cagiva Navigator (still testing)/TLS1000

### Whats in side? ###

* A breadboard diagram of the needed circuit
* A Fritzing file with the PCB (unfinished), breadboard and circuit diagrams 
* Arduino source code

### Components Required? ###

* 1x 10k ohm resistor (arduino pull up)
* 1x ATMega 328 (and some way of programming it ISP/serial)
* 1x 7 Segment Display (I used common cathode)
* 7x 220 ohm resistors (LED current limiters)
* 2x 22pF ceramic capacitors
* 1x 16mhz crystal
* 2x 470k ohm resistors (Voltage divider)
* 1x LM2940-5v (TO-220 5v regulator)
* 1x 22uF electrolytic capacitor
* 1x 0.47uF electrolytic capacitor

See the Design folder for the circuit.

### Building the code ###

* Use the Arduino IDE to build and program the chip
* Ensure the correct bike is defined in the header file

### Thanks ###

*  http://www.v-strom.co.uk/ TLPower and Tall Paul for helping with testing.
*  Stromputer http://stromputer.wikispaces.com/  